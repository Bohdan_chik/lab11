#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Load the dictionary of words from the given filename.
// Return a pointer to the array of strings.
// Sets the value of size to be the number of valid
// entries in the array (not the total array length).
char ** loadDictionary(char *filename, int *size)
{
	FILE *in = fopen(filename, "r");
	if (!in)
	{
	    perror("Can't open dictionary");
	    exit(1);
	}
	
	// Read the dictionary line by line.
    int arrLength = 100;
    char line[100];
    *size = 0;

	// Allocate memory for an array of strings (arr).
    char ** arr = malloc(sizeof(char*) * arrLength);

    while(fgets(line, 100, in) != NULL)
    {
        // Trim New Line
        char *p = strchr(line, '\n');
        if (p) *p = '\0';

	    // Allocate memory for the string (line)
        if (*size < arrLength) 
        {
            arr[*size] = malloc(sizeof(char) * (strlen(line) + 1));
            strcpy(arr[*size], line);
        } 
        else
        {
            // Make arrLenght bigger
            arrLength += 100;
            arr = realloc(arr, sizeof(char*) * arrLength);
            arr[*size] = malloc(sizeof(char) * (strlen(line) + 1));
            strcpy(arr[*size], line);
        }
        (*size)++;   
    }
    fclose(in);
    return arr;
}

// Search the dictionary for the target string
// Return the found string or NULL if not found.
char * searchDictionary(char *target, char **dictionary, int size)
{
    if (dictionary == NULL) return NULL;
    
	for (int i = 0; i < size; i++)
	{
	    if (strcmp(target, dictionary[i]) == 0)
	    {
	        return dictionary[i];
	    }
	}
	return NULL;
}